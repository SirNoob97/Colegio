package menus;

import java.util.Scanner;

/**
 * Clase creada para mostrar datos y menus de la app
 * --pendiente--
 * @author martin
 *
 */
public class Menus {
	/**
	 * metodo menu para la actualizacon de registros
	 */
	public static void menuActualizarAlumno() {
		System.out.println("------------------");
		System.out.println("|Actualizar Datos|");
		System.out.println("------------------\n");

		System.out.println("1)Editar los nombres del alumno.");
		System.out.println("2)Editar los apellidos del alumno.");
		System.out.println("3)Editar el curso del alumno.");
		System.out.println("4)Editar la nota del alumno.");
		System.out.println("5)Editar las observaciones del alumno.");
		System.out.println("6)Editar todo el registro del alumno.");
		System.out.println("0)Atras");
	}

	/**
	 * metodo menu principal para la gestion de los registro de alumnos
	 */
	public static void menuAlumnos() {
		System.out.println("--------------------");
		System.out.println("|Gestion de Alumnos|");
		System.out.println("--------------------\n");

		System.out.println("1)Añadir registro de alumno.");
		System.out.println("2)Ver registro de alumno.");
		System.out.println("3)Editar registro de alumno.");
		System.out.println("4)Eliminar registro de alumno.");
		System.out.println("5)Visualizar todos los registros.");
		System.out.println("0)Salir");

	}

	/**
	 * metodo parvisualizar una pausa en la ejecucion de la app. Ser notificado
	 * cuando se realizo una operacion y salir de la app
	 */
	public static void continuar() {
		Scanner pausa = new Scanner(System.in);
		System.out.println("\nPresione ENTER para continuar.");
		pausa.nextLine();

	}

}
