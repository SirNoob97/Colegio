package modelos;

public abstract class Persona {

	private String DNI;
	private String nombres;
	private String apellidos;
	
	public Persona() {
		
	}
	
	public Persona(String DNI, String nombres, String apellidos) {
		super();
		this.DNI = DNI;
		this.nombres = nombres;
		this.apellidos = apellidos;
	}

	public String getDNI() {
		return DNI;
	}

	public void setDNI(String DNI) {
		this.DNI = DNI;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getNombres() {
		return nombres;
	}
	
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	
	@Override
	public String toString() {
		return "DNI: " + DNI + "\nNombres: " + nombres + "\nApellidos: " + apellidos;
	}
}
