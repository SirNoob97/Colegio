package modelos;

public class Alumno extends Persona{

	private String curso;
	private String notaFinal;
	private String observaciones;
	
	public Alumno() {

	}

	public Alumno(String DNI, String nombres, String apellidos, String curso, String notaFinal, String observaciones) {
		super(DNI, nombres, apellidos);
		this.curso = curso;
		this.notaFinal = notaFinal;
		this.observaciones = observaciones;		
	}



	public String getCurso() {
		return curso;
	}

	public void setCurso(String curso) {
		this.curso = curso;
	}

	public String getNotaFinal() {
		return notaFinal;
	}

	public void setNotaFinal(String notaFinal) {
		this.notaFinal = notaFinal;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	
	@Override
	public String toString() {
		return super.toString() + "\nCurso: " + curso
				+ "\nNota: " + notaFinal + "\nObservaciones: " + observaciones;
	}
}
/*
alumnos.entrySet().stream().filter(k -> k.equals(id))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

*/