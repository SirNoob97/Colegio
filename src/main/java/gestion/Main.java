package gestion;


import menus.Menus;

/**
 * Clase principal para la ejecucion de la aplicacion
 * 
 * @author martin
 *
 */
public class Main {
	public static void main(String[] args) {
		int opcion;
		do {

			do {
				Menus.menuAlumnos();
				opcion = LecturaDeDatos.leerNumeroEntero("Que desea hacer?");
				System.out.println();
				if (opcion < Constantes.SALIR || opcion > Constantes.VERTODO) {
					System.out.println("Solo se admiten valores entre 0 y 4!!!");
				}
			} while (opcion < Constantes.SALIR || opcion > Constantes.VERTODO);

			if (opcion == Constantes.SALIR) {
				Menus.continuar();
				break;
			}

			switch (opcion) {
			case Constantes.CREAR:
				
				GestionAlumnos.agregarAlumno();

				break;
			case Constantes.BUSCAR:
				
				if (GestionAlumnos.verificarAlumnos()) {
					System.out.println("El registro de alumnos esta vacio!!!!");
					Menus.continuar();
					continue;
				}
				
				String idBuscar = String.valueOf(LecturaDeDatos.leerNumeroEntero("Ingrese el ID del alumno a visualizar"));
				GestionAlumnos.mostrarAlumno(idBuscar);
				Menus.continuar();

				break;
			case Constantes.EDITAR:

				if (GestionAlumnos.verificarAlumnos()) {
					System.out.println("El registro de alumnos esta vacio!!!!");
					Menus.continuar();
					continue;
				}

				int opcion2;
				do {
					do {
						Menus.menuActualizarAlumno();
						opcion2 = LecturaDeDatos.leerNumeroEntero("Que desea hacer?");
						System.out.println();

						if (opcion2 < Constantes.SALIR || opcion2 > Constantes.ACTUALIZACION_COMPLETA)
							System.out.println("Solo se admiten valores entre 0 y 4!!!");
					} while (opcion2 < Constantes.SALIR || opcion2 > Constantes.ACTUALIZACION_COMPLETA);

					if (opcion2 == Constantes.SALIR) {
						break;
					}
					
					String idEditar = String.valueOf(LecturaDeDatos.leerNumeroEntero("Ingrese el ID del alumno a editar."));
					GestionAlumnos.actualizarDatos(opcion2, idEditar);

				} while (true);

				break;
			case Constantes.ELIMINAR:

				if (GestionAlumnos.verificarAlumnos()) {
					System.out.println("El registro de alumnos esta vacio!!!!");
					Menus.continuar();
					continue;
				}
				
				String idEliminar = String.valueOf(
						LecturaDeDatos.leerNumeroEntero("Ingrese el ID del alumno que eliminara del registro"));
					GestionAlumnos.eliminarAlumno(idEliminar);
				
				break;
			case Constantes.VERTODO:

				if (GestionAlumnos.verificarAlumnos()) {
					System.out.println("El registro de alumnos esta vacio!!!!");
					Menus.continuar();
					continue;
				}
				GestionAlumnos.verTodoElRegistro();
				
				break;
			}

		} while (true);

	}

}
