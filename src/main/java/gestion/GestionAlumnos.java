package gestion;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.HashMap;
import java.util.stream.Collectors;

import modelos.Alumno;

import static menus.Menus.continuar;

/**
 * Clase creada como intermediaria entre la clse alumno y la clase main se
 * encarga de hacer las operaciones en el registro
 *
 * @author martin
 */
public class GestionAlumnos {
    private static final Map<Alumno, List<String>> alumnos = new HashMap<>();

    /**
     * verificacion del registro de alumnos
     *
     * @return se retornara un boolean para confirmar si el registro esta vacio o no
     */
    public static boolean verificarAlumnos() {
        return alumnos.isEmpty();
    }

    /**
     * metodo para buscar o comprobar si existe un registro
     *
     * @param id es enviado como parametro a la funcion para realizar la
     *           busqueda del registro que coincida con ese id
     * @return se retornara el alumno devuelto por el metodo que realiza la busqueda
     * en la base de datos
     */
    public static Alumno buscarAlumnoId(String id) {
        return alumnos.keySet().stream().filter(k -> k.getDNI().equals(id)).findFirst().orElse(null);
    }

    /**
     * metodo para agregar alumnos al registro
     */
    public static void agregarAlumno() {
        String id;
        String nombres;
        String apellidos;
        String nota;
        String curso;
        List<String> modulos = new ArrayList<>();
        String observaciones;

        Alumno alumno;

        System.out.println("----------------\n|Agragar Alumno|\n----------------\n");

        do {
            id = String.valueOf(LecturaDeDatos.leerNumeroEntero("Ingrese el ID del alumno"));
            alumno = buscarAlumnoId(id);

            if (alumno != null)
                System.out.printf("\nYa existe un alumno con el ID %s en el registro\n\n", id);
        } while (alumno != null);

        nombres = LecturaDeDatos.leerCadenadeCaracteres("Ingrese los nombres del alumno");
        apellidos = LecturaDeDatos.leerCadenadeCaracteres("Ingrese los apellidos del alumno");

        for (int i = 0; i < 5; i++) {
            modulos.add(LecturaDeDatos.leerCadenadeCaracteres("Ingrese el modulo del alumno"));
        }

        curso = String.valueOf(LecturaDeDatos.leerNumeroEntero("Ingrese el curso del alumno"));
        nota = String.valueOf(LecturaDeDatos.leerNumeroReal("Ingrese la nota del alumno"));
        observaciones = LecturaDeDatos.leerCadenadeCaracteres("Ingrese las observaciones del alumno");

        alumno = new Alumno(id, nombres, apellidos, curso, nota, observaciones);

        alumnos.put(alumno, modulos);
        System.out.println("\nAlumno agregado exitosamente al registro.");
        continuar();

    }

    /**
     * metodo para visualizar un registro
     *
     * @param DNI metodo recivido para buscar el alumno que se desea mostrar
     */
    public static void mostrarAlumno(String DNI) {
        Alumno alumno = buscarAlumnoId(DNI);

        if (alumno != null) {

            System.out.println("\n------------------\n|Datos del Alumno|\n------------------\n");
            System.out.println(alumno);

            alumnos.entrySet().stream()
                    .filter(alumn -> alumno.equals(alumn.getKey()))
                    .map(Map.Entry::getValue)
                    .findFirst()
                    .ifPresent((modulos) -> System.out.println(formatoModulos(modulos)));

        } else {
            System.out.println("\nEl alumno no existe en el registro\n");
        }
    }

    /**
     * metodo para convertir una lista en un string
     *
     * @param modulos lista que se recive para ser convertida una cadena de texto
     * @return la cadena de texto para ser imprimida en pantalla
     */
    private static StringBuffer formatoModulos(List<String> modulos) {
        StringBuffer aux = new StringBuffer("\nModulos: ");
        aux.append(modulos.toString().replace("[", "").replace("]", ""));
        return aux;
    }

    /**
     * metodo para visualizar el registro de alumnos
     */
    public static void verTodoElRegistro() {
        for (Entry<Alumno, List<String>> key : alumnos.entrySet()) {
            System.out.println("\n------------------\n|Datos del Alumno|\n------------------\n");
            System.out.println(key.getKey().toString() + "\nModulos: "
                    + key.getValue().toString().replace("[", "").replace("]", ""));
        }
        continuar();
    }

    /**
     * metodo para la actualizacion de datos del registro
     *
     * @param opcion parametro recivido para especificar que campo del registro se
     *               quiere actualizar
     */
    public static void actualizarDatos(int opcion, String idEditar) {

        Alumno alumno = buscarAlumnoId(idEditar);
        if (alumno != null) {

            switch (opcion) {
                case Constantes.CAMBIO_NOMBRES:
                    mostrarAlumno(idEditar);
                    System.out.println();
                    String nombres = LecturaDeDatos.leerCadenadeCaracteres("Ingrese los nuevos nombres del alumno");
                    alumno.setNombres(nombres);
                    System.out.println("\nNombres actualizados.\n");
                    mostrarAlumno(idEditar);
                    continuar();

                    break;
                case Constantes.CAMBIO_APELIIDOS:
                    mostrarAlumno(idEditar);
                    System.out.println();
                    String apellidos = LecturaDeDatos.leerCadenadeCaracteres("Ingrese los nuevos apellidos del alumno");
                    alumno.setApellidos(apellidos);
                    System.out.println("\nApellidos actualizados.\n");
                    mostrarAlumno(idEditar);
                    continuar();

                    break;
                case Constantes.CAMBIO_CURSO:
                    mostrarAlumno(idEditar);
                    System.out.println();
                    String curso = LecturaDeDatos.leerCadenadeCaracteres("Ingrese el nuevo curso del alumno");
                    alumno.setCurso(curso);
                    System.out.println("\nCurso actualizado.\n");
                    mostrarAlumno(idEditar);
                    continuar();

                    break;
                case Constantes.CAMBIO_NOTA:
                    mostrarAlumno(idEditar);
                    System.out.println();
                    double nota = LecturaDeDatos.leerNumeroReal("Ingrese la nueva nota del alumno");
                    alumno.setNotaFinal(String.valueOf(nota));
                    System.out.println("\nNota actualizada.\n");
                    mostrarAlumno(idEditar);
                    continuar();

                    break;
                case Constantes.CAMBIO_OBSERVACIONES:
                    mostrarAlumno(idEditar);
                    System.out.println();
                    String observaciones = LecturaDeDatos.leerCadenadeCaracteres("Ingrese las nuevas observaciones del alumno");
                    alumno.setObservaciones(observaciones);
                    System.out.println("\nObservaciones actualizadas.\n");
                    mostrarAlumno(idEditar);
                    continuar();

                    break;
                case Constantes.ACTUALIZACION_COMPLETA:
                    mostrarAlumno(idEditar);
                    System.out.println();
                    nombres = LecturaDeDatos.leerCadenadeCaracteres("Ingrese los nombres del alumno");
                    apellidos = LecturaDeDatos.leerCadenadeCaracteres("Ingrese los apellidos del alumno");
                    curso = LecturaDeDatos.leerCadenadeCaracteres("Ingrese el curso del alumno");
                    nota = LecturaDeDatos.leerNumeroReal("Ingrese la nota del alumno");
                    observaciones = LecturaDeDatos.leerCadenadeCaracteres("Ingrese las observaciones del alumno");
                    alumno.setNombres(nombres);
                    alumno.setApellidos(apellidos);
                    alumno.setCurso(curso);
                    alumno.setNotaFinal(String.valueOf(nota));
                    alumno.setObservaciones(observaciones);
                    System.out.println("\nRegistro actualizado.\n");
                    mostrarAlumno(idEditar);
                    continuar();

            }
        } else
            System.out.println("\nEl alumno no existe en el registro\n");
    }

    /**
     * metodo para eliminar registro
     *
     * @param idEliminar parametro recivido para especificar cual registro se quiere
     *                   eliminar
     */
    public static void eliminarAlumno(String idEliminar) {
        Alumno alumno = buscarAlumnoId(idEliminar);
        if (alumno != null) {
            alumnos.entrySet().removeIf(a -> alumno.equals(a.getKey()));
            System.out.println("\nEl alumno fue eliminado del registro!!!\n");
        } else
            System.out.println("\nEl alumno no existen en el registro!!!\n");
    }

}
