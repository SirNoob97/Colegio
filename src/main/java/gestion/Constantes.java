package gestion;

/**
 * Constates utilizadas en los menus para la gestion de registro de estudiantes
 * y para evitar confusiones
 * 
 * @author martin
 *
 */
public interface Constantes {
	/**
	 * Constantes para el menu pricipal de la aplicacion
	 */
	final int CREAR = 1, BUSCAR = 2, EDITAR = 3, ELIMINAR = 4, VERTODO = 5, SALIR = 0;

	/**
	 * Constantes para el menu de edicion del registro de un estudiante
	 */
	final int CAMBIO_NOMBRES = 1, CAMBIO_APELIIDOS = 2, CAMBIO_CURSO = 3, CAMBIO_NOTA = 4, 
			CAMBIO_OBSERVACIONES = 5, ACTUALIZACION_COMPLETA = 6;

}
