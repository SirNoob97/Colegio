package gestion;

import java.util.Scanner;

public class LecturaDeDatos {
    static Scanner leer = new Scanner(System.in);

    /**
     * metodo para comprobar que los datos introducidos por teclado no sean cadenas
     * vacias
     *
     * @param mensaje parametro recivido para especificar el dato que se debe
     *                introducir
     * @return se retornara el dato introducido siempre que este no sea una cadena
     * vacia
     */
    // --->Agregar mas validaciones en el futuro<---
    public static String leerCadenadeCaracteres(String mensaje) {
        String cadena;

        while (true) {
            System.out.printf("%s: ", mensaje);
            cadena = leer.nextLine().strip();

            if (!cadena.isEmpty())
                return cadena;

            System.out.print("\nEl campo no puede quedar vacio!!!\n");
        }
    }

    /**
     * metodo para comprobar que el dato introducido por teclado es un numero entero
     * y positivo
     *
     * @param mensaje mensaje parametro recivido para especificar el dato que se
     *                debe introducir
     * @return se retornara el dato introducido siempre que este sea un numero
     * entero
     */
    public static int leerNumeroEntero(String mensaje) {
        int numero;
        while (true) {
            try {
                System.out.printf("%s: ", mensaje);
                numero = Integer.parseInt(leer.nextLine());
            } catch (NumberFormatException e) {
                System.out.println("\nSolo se adminten numero enteros!!!\n");
                continue;
            }
            if (numero >= 0)
                return numero;
            else {
                System.out.println("\nSolo se admiten numeros positivos!!!\n");
            }
        }

    }

    /**
     * metodo para comprobar que el dato introducido por teclado es un numero real y
     * positivo
     *
     * @param mensaje mensaje mensaje parametro recivido para especificar el dato
     *                que se debe introducir
     * @return se retornara el dato introducido siempre que este sea un numero real
     */
    public static double leerNumeroReal(String mensaje) {
        double numero;
        while (true) {
            try {
                System.out.printf("%s: ", mensaje);
                numero = Double.parseDouble(leer.nextLine());
            } catch (NumberFormatException e) {
                System.out.println("\nSolo se adminten numero reales!!!\n");
                continue;
            }
            if (numero >= 0.0)
                return numero;
            else {
                System.out.println("\nSolo se admiten numeros positivos!!!\n");
            }
        }
    }
}
